#!/bin/sh

(sudo -u postgres psql jiradb <<EOF
COPY (SELECT DISTINCT t.table_name
FROM
  information_schema.tables as t,
  information_schema.columns as c
WHERE
  t.table_schema NOT IN ('pg_catalog', 'information_schema')
  AND t.table_name LIKE 'AO_AEFED0_%'
  AND t.table_name = c.table_name
  AND c.column_name LIKE '%ATED_AT')
TO STDOUT CSV;
EOF
) | while read line; do
sudo -u postgres psql jiradb <<EOF
DELETE FROM "$line";
COPY "$line" FROM '/tmp/$line.csv' CSV;
EOF
done
